from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse

from .models import Ticket, User, Queue, Requesters, Comment, StatusUpdate

# Non-Admin users will get the same home page that varies by their groups and tickets
# The page for viewing a ticket will display the data of the specified ticket only

# Home page for a user
def index(request):
	ql = request.user.groups.values_list('name', flat = True)
	ts = request.user.ticket_set.exclude(status='CLOSED')
	tn = ts.values_list('Subject', flat = True)
	tk = ts.values_list('id', flat = True)
	tq = []
	for q in request.user.groups.all():
		tq.extend( q.ticket_set.exclude(status='CLOSED') )
		
	context = {'user':request.user, 'qlist': ql, 'tlist':tn, 'tkeys':tk, 'tqueues':tq}
	return render(request, 'RequestTracker/index.html', context)

# Allows user to view a ticket if they may
def ticket(request, ticket_id):
	tkt = get_object_or_404(Ticket, pk=ticket_id)
	if  tkt.requester.id == request.user.id or tkt.queue in request.user.groups.all():
		c = Comment.objects.filter(ticket=tkt).order_by('-comment_time')
		u = StatusUpdate.objects.filter(ticket=tkt).order_by('-time')
		context = {'user':request.user, 'ticket': tkt, 'cmt':c, 'upd':u }
		return render(request,  'RequestTracker/ticket.html', context)
	else:
		return render(request,  'RequestTracker/index.html')
	
# Allows user to add a comment to a ticket
def addcomment(request, ticket_id):
	tkt = get_object_or_404(Ticket, pk=ticket_id)
	cText = request.POST['ncomment']
	cmmnt = Comment(commentText=cText, commentor=request.user, ticket = tkt)
	cmmnt.save()
	return HttpResponseRedirect(reverse('Ticket', args=(tkt.pk,) ))
	
# Allows user to create a ticket
def makeRequest(request):
	ql = request.user.groups.values_list('name', flat = True)
	context = {'qlist':ql}
	return render(request, 'RequestTracker/makeRequest.html', context)

# Processes a submitted ticket
# If successful: New Ticket, unsucessful: Ticket creation page.
def submitRequest(request):
	try:
		new_ticket = Ticket( Subject = request.POST['subject'], 
						Description = request.POST['description'], 
						queue = Queue.objects.get(name = request.POST['target']),
						requester = request.user
						)
		new_ticket.save()
		
	except:
		return makeRequest(request)
	else:
		return HttpResponseRedirect(reverse('Ticket', args=(new_ticket.pk,) ))

# Process a status update
def updateStatus(request, ticket_id):
	tkt = get_object_or_404(Ticket, pk=ticket_id)
	uStat = request.POST['nstatus']
	updt = StatusUpdate(status=uStat, updater=request.user, ticket = tkt)
	updt.save()
	return HttpResponseRedirect(reverse('Ticket', args=(tkt.pk,) ))

# Allow a worker to take a ticket
def taketicket(request, ticket_id):
	tkt = get_object_or_404(Ticket, pk=ticket_id)
	wrkr = request.user
	tkt.worker = wrkr
	u = StatusUpdate('assigned', updater=request.user, ticket = tkt)
	tkt.save()
	u.save()
	return HttpResponseRedirect(reverse('Ticket', args=(tkt.pk,) ))
