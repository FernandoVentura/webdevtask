from django.conf.urls import url

from . import views

urlpatterns = [
	url( r'^$', views.index, name='Index' ),
	url( r'^ticket/(?P<ticket_id>[0-9]+)/$', views.ticket, name='Ticket' ),
	url( r'^makeRequest/$', views.makeRequest, name='Make Request' ),
	url( r'^submitRequest/$', views.submitRequest, name='submitRequest' ),
	url( r'^addcomment/(?P<ticket_id>[0-9]+)/$', views.addcomment, name='addcomment' ),
	url( r'^updatestatus/(?P<ticket_id>[0-9]+)/$', views.updateStatus, name='updateStatus' ),
	url( r'^taketicket/(?P<ticket_id>[0-9]+)/$', views.taketicket, name='claimticket' ),
]