from django.contrib import admin
from django.contrib.auth.models import  Group
from .models import User, Queue, Ticket, Comment, StatusUpdate, Requesters

admin.site.unregister(Group)
admin.site.register(User)
admin.site.register(Queue)
admin.site.register(Requesters)
admin.site.register(Ticket)
admin.site.register(Comment)
admin.site.register(StatusUpdate)

