from __future__ import unicode_literals
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import  Group
from django.db import models

from django.core.mail import send_mail


# From email address, should be altered to a real one:
origin = 'no-reply@example.com'
# Dummy domain name, should be altered to a real one:
domain = 'localhost:8000'


#Possible Status ipdates
STATUS_OPTIONS = (
	( 'new', 'NEW' ),
	( 'open', 'OPEN' ),
	( 'assigned', 'ASSIGNED' ),
	( 'closed', 'CLOSED' ),
)

# Users extended here for easier later modification if required

class User(AbstractUser):
	pass


# Queue Model, proxy of Auth Groups - Foreign Key will be held by tickets and Requesters.
# Members of Queues are Workers

class Queue(Group):
	class Meta:
		proxy = True
		app_label = 'auth'
		verbose_name = 'Queue'
	

# Requesters model - Defines who may open tickets on a group.
# Opted for this simpler approach instead of extending groups.
# A Requesters instance must thus be created before anyone may open tickets.
class Requesters(models.Model):
	everyone = models.BooleanField(default=True)
	queue = models.OneToOneField(Group)
	allowed = models.ManyToManyField(User)
	def __str__(self):
		return 'Allowed requester on Queue ' + self.queue.name


# Ticket Model - Contains Basic Ticket structure, but no updates

class Ticket(models.Model):
	Subject = models.CharField(max_length=128)
	Description = models.TextField(max_length=1024)
	status = models.CharField(max_length=10, choices=STATUS_OPTIONS, default='new' )
	create_date = models.DateTimeField('Date and time created', auto_now_add = True)
	queue = models.ForeignKey(Group, on_delete=models.CASCADE)
	requester = models.ForeignKey(User, on_delete=models.CASCADE)
	worker = models.ForeignKey(User, on_delete=models.CASCADE, related_name='worker', null=True, blank=True)
	def save(self, *args, **kwargs):
		
		super(Ticket, self).save(*args, **kwargs)
		
		# Notify workers via email:
		hmessage = '"' + self.Subject + '"<br/>'
		hmessage += 'This ticket arrived in queue '+ self.queue.name 
		hmessage +='. You may access the ticket <a href="http://'
		hmessage += domain + '/RequestTracker/ticket'+ str(self.id)+ '/'
		hmessage += '">Here.</a>'
		
		message = '"' + self.Subject + '" ~ '+ 'This ticket arrived in queue '+ self.queue.name 
		message +='. You may access the ticket at '
		message += domain + '/RequestTracker/ticket'+ str(self.id) + '/'
		
		send_mail(
			'New Ticket',
			message,
			origin,
			self.queue.user_set.values_list('email', flat=True),
			html_message=hmessage,
			fail_silently=False
		)
		
	
	def __str__(self):
		return 'Ticket: "' + self.Subject + '" ~' + self.requester.username



# Comments on Tickets

class Comment(models.Model):
	commentText = models.TextField(max_length=512)
	comment_time = models.DateTimeField('Time of Comment', auto_now_add=True)
	commentor = models.ForeignKey(User, on_delete=models.CASCADE)
	ticket = models.ForeignKey(Ticket, on_delete=models.CASCADE)
	def __str__(self):
		return self.commentor.username + ' on ' + self.ticket.Subject
	

# Status updates for ticket, only updates, not initial status.

# Note: The latest status update is not guaranteed to be the current status.
# This allows for certain system expansions, such a future CLOSED status 
# update when a ticket should automatically expire after a certain period

class StatusUpdate(models.Model):
	status = models.CharField(max_length=10,  choices=STATUS_OPTIONS, default='new' )
	time = models.DateTimeField('Time of Change', auto_now_add=True)
	ticket = models.ForeignKey(Ticket, on_delete=models.CASCADE)
	updater = models.ForeignKey(User, on_delete=models.CASCADE)
	def save(self, *args, **kwargs):
		self.ticket.status = self.status
		self.ticket.save()
		super(StatusUpdate, self).save(*args, **kwargs)
	
	def __str__(self):
		return 'Update on ' + self.ticket.Subject
	
#tk = Ticket.objects.get(pk=self.ticket)
		